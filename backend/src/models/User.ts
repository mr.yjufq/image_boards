import {Schema, model, Model} from "mongoose";
import * as bcrypt from 'bcrypt';
import {nanoid} from "nanoid";

const SALT_WORK_FACTOR = 10;

interface IUser {
    _id?: string;
    username: string;
    password: string;
    token: string;
    checkPassword: (password: string) => Promise<boolean>;
    generateToken: () => void;
}


const UserSchema = new Schema<IUser, Model<IUser, object>>({
    username: {
        type: String,
        required: true,
        unique: true,
        validator: async (username: string) => {
            const user = await User.findOne({username});
            if (user) {
                return false;
            }
        },
        message: 'This user is already registered',
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
    },
    token: String
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);

    next();
});

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = function (password: string) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

const User: Model<IUser> = model('User', UserSchema);

export default User;