import mongoose, {Schema} from "mongoose";

const ImageBoardSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    image: {
        type: String,
        required: true
    },
});

const ImageBoard = mongoose.model("ImageBoard", ImageBoardSchema);


export default ImageBoard;