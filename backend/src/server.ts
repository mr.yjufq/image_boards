import express from 'express';
import logger from 'jet-logger';
import cors from 'cors';
import mongoose from 'mongoose';
import process from 'process';
import imageBoard from "@src/controllers/imageBoards.controller"
import usersController from "@src/controllers/users.controller";

const app = express();
const PORT = 8005;

const run = async () => {
    await mongoose.connect('mongodb://127.0.0.1/Exam-12');
    app.listen(PORT, () => {
        logger.info(`Server is running on http://localhost:${PORT}`);
    });
    process.on('exit', () => {
        mongoose.disconnect();
    });
};

app.use(express.static('src/public/uploads'));
app.use(express.json());
app.use(cors());

app.use('/boards', imageBoard);
app.use('/users', usersController);

run().catch(e => logger.err(e));