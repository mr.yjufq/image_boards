import {Request, Response, Router} from "express";
import User from "@src/models/User";
import {AuthCredentialsDto} from "@src/dto/AuthCredentials.dto";
import authMiddleware from "@src/middlewares/auth.middleware";
import {IRequest} from "@src/interfaces/IRequest";

const controller = Router();

controller.post('/', async (req: Request, res: Response) => {
    const user = new User(req.body as AuthCredentialsDto);
    user.generateToken();

    try {
        await user.save();
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
});

controller.post('/login', async (req: Request, res: Response) => {
    const {password, username} = req.body as AuthCredentialsDto;
    const user = await User.findOne({username});

    if (!user) {
        return res
            .status(400)
            .send({error: 'Username or password incorrect'});
    }

    if (password) {
        const isMatch = await user.checkPassword(password);

        if (!isMatch) {
            return res
                .status(400)
                .send({error: 'Username or password incorrect'});
        }

        try {
            user.generateToken();
            await user.save();

            res.send(user);
        } catch (e) {
            res
                .status(400)
                .send({error: e.message});
        }
    } else {
        res.status(400).send({error: 'Password is required'});
    }
});

controller.delete('/logout', authMiddleware, async (req: IRequest, res) => {
    req.user.token = null;
    req.user.save();

    res.send({message: 'Success'});
});
export default controller;