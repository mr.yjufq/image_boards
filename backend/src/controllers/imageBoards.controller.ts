import multer from 'multer';
import {uploadPath} from "@src/config";
import path from "path";
import {nanoid} from "nanoid";
import express, {Request, Response, Router} from "express";
import ImageBoard from "@src/models/ImageBoard";
import authMiddleware from "@src/middlewares/auth.middleware";
import {CreateImageBoardDto} from "@src/dto/CreateImageBoard.dto";
import {IRequest} from "@src/interfaces/IRequest";


const storage: multer.StorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload: multer.Multer = multer({storage});

const controller: Router = express.Router();


controller.get('/', async (req: Request<{}, {}, {}, { user_id: string }>, res: Response) => {
    try {
        let {query} = req;

        if (query.user_id) {
            query = {user_id: req.query.user_id};
            const result = await ImageBoard.find(query).populate('user_id', "username");

            res.send(result);
        } else {
            const result = await ImageBoard.find().populate('user_id', "username");
            res.send(result);
        }
    } catch (e) {
        res.sendStatus(500).send({message: 'Not found'});
    }
});

controller.post('/', [authMiddleware, upload.single('image')], async (req: IRequest, res: Response) => {
    try {
        const {title} = req.body as CreateImageBoardDto;
        let {image} = req.body as CreateImageBoardDto;

        if (req.file) {
            image = req.file.filename;
        }

        const imageBoard: CreateImageBoardDto = new CreateImageBoardDto(title, image, req.user._id);
        const result = new ImageBoard(imageBoard);
        await result.save();
        res.send(result);
    } catch (e) {
        res.status(400).send(e);
    }
});

controller.delete('/:id', authMiddleware, async (req: IRequest, res: Response) => {
    const {id} = req.params;

    try {
        await ImageBoard.deleteOne({_id: id, user_id: req.user._id});
        res.status(200).send('Delete image Board by ID');
    } catch (e) {
        res.status(500).send(e.message);
    }
});


export default controller;



