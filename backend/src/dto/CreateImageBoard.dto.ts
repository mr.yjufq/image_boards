export class CreateImageBoardDto {
    title: string;
    user_id?: string;
    image: string

    constructor(title: string, image: string, user_id?: string) {
        this.title = title;
        this.user_id = user_id;
        this.image = image;
    }
}
