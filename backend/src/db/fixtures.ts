import mongoose from 'mongoose';
import ImageBoard from "../models/ImageBoard";
import User from "../models/User";


mongoose.connect('mongodb://127.0.0.1/Exam-12');
const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('imageboards');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user1, user2] = await User.create([
        {
            username: "Artem",
            password: "123",
            token: null,
        },
        {
            username: "User",
            password: "123",
            token: null,
        }
    ]);


    await ImageBoard.create({
        title: "Fairy tail",
        user_id: user1._id,
        image: "fairyTail.jpe"
    }, {
        title: "Tanjiro",
        user_id: user1._id,
        image: "tanjiro.jpg"
    }, {
        title: "Lucy Heartfilia",
        user_id: user1._id,
        image: "lucy.jpg"
    }, {
        title: "One Piece",
        user_id: user2._id,
        image: "onePiece.jpg"
    }, {
        title: "Naruto",
        user_id: user2._id,
        image: "naruto.png"
    });

    db.close();
});
