import {configureStore, Middleware} from "@reduxjs/toolkit";
import {Provider} from "react-redux";
import {AnyAction, Dispatch, Store} from "redux";
import App from "./App";
import ReactDOM from "react-dom/client";
import UsersSlices, {IUserState} from "./store/services/userSlice";
import imageBoardSlice, {IImageBoardsState} from "./store/services/imageBoardsSlice";

export type RootState = {
    imageBoards?: IImageBoardsState;
    usersState: IUserState;
};

export type AppDispatch = typeof store.dispatch;

const localStorageMiddleware =
    (store: Store) => (next: Dispatch<AnyAction>) => (action: AnyAction) => {
        const result = next(action);
        const state = store.getState();
        localStorage.setItem("user", JSON.stringify(state.usersState.user));
        return result;
    };

const reHydrateStore = (): RootState | undefined => {
    const userString: string | null = localStorage.getItem("user");
    if (userString !== null) {
        const parsedUser = JSON.parse(userString);
        const reHydratedState: RootState = {
            usersState: {
                user: parsedUser,
                isLoading: false,
                errorRegister: null,
                errorLogin: null,
                logoutError: null,
            },
        };
        return reHydratedState;
    }
    return undefined;
};

const store = configureStore({
    reducer: {
        imageBoards: imageBoardSlice,
        usersState: UsersSlices,
    },
    preloadedState: reHydrateStore(),
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(localStorageMiddleware as Middleware),
});

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);

root.render(
    <Provider store={store}>
        <App/>
    </Provider>
);