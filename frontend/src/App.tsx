import React from 'react';
import LayoutComponent from "./components/Layout/LayoutComponent";
import instance from "./API/createAxios";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import ShowImageBoards from "./containers/ShowImageBoards/ShowImageBoards";
import Login from "./containers/Auth/Login/Login";
import Register from "./containers/Auth/Register/Register";
import ShowAuthorBoards from "./containers/ShowAuthorBoards/ShowAuthorBoards";
import AddForm from "./containers/AddForm/AddForm";
import {useSelector} from "react-redux";
import {RootState} from "./index";
import {IUser} from "./store/services/userSlice";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";


function App() {
    const user = useSelector<RootState, IUser | null>(state => state.usersState.user);
    instance.interceptors.request.use((config) => {
        const userString: string | null = localStorage.getItem("user");
        if (userString !== null) {
            const parsedUser = JSON.parse(userString);
            if (parsedUser) {
                config.headers.Authorization = parsedUser.token;
            }
            return config;
        }
        return config;
    }, (error) => {
        return Promise.all(error);
    })

    const AddFormProtected = <ProtectedRoute
        isAllowed={!!user}
        redirectPath='/'
    >
        <AddForm/>
    </ProtectedRoute>;

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<LayoutComponent/>}>
                    <Route index element={<ShowImageBoards/>}/>
                    <Route path='author/:id' element={<ShowAuthorBoards/>}/>
                    <Route path='author/:id/add-form' element={AddFormProtected}/>
                </Route>
                <Route path="login" element={<Login/>}/>
                <Route path="register" element={<Register/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
