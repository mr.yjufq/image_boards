import React, {ChangeEvent, ChangeEventHandler} from 'react';
import {Typography} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../../index";
import {changeFileName} from "../../../store/services/imageBoardsSlice";
import { UploadOutlined } from '@ant-design/icons';
import './FileUpload.css';

const {Text} = Typography;

interface IProps {
    onChange: ChangeEventHandler<HTMLInputElement>;
    name: string;
}

const FileUpload = ({name, onChange}: IProps) => {
    const dispatch = useDispatch<AppDispatch>();
    const fileName = useSelector<RootState, string | undefined>(state => state.imageBoards?.filename);

    const onFileChange = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            dispatch(changeFileName(e.target.files[0].name))
        } else {
            dispatch(changeFileName(''));
        }
        onChange(e);
    }

    return (
        <div className='upload_block'>
            <label className="input-file">
                <input
                    name={name}
                    hidden
                    accept="image/*"
                    type="file"
                    onChange={onFileChange}
                />
                <span className="input-file-btn"><UploadOutlined /> Choose file</span>
            </label>
            <Text style={{color: "whitesmoke", fontSize: 15}}>{fileName}</Text>
        </div>
    )
};

export default FileUpload;