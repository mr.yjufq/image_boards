import React, {useState} from 'react';
import {Card, Col, Modal} from 'antd';
import './ImageBoard.css';
import {CloseOutlined} from '@ant-design/icons';
import {Link, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {RootState} from "../../index";
import {IUser} from "../../store/services/userSlice";
import { DeleteOutlined } from '@ant-design/icons';
import {pathLocalHost} from "../../constants/pathLocalHost";

const {Meta} = Card;

interface IProps {
    author: string,
    title: string,
    image: string,
    path: string,
    without?: boolean
    onDeleteHandler?: React.MouseEventHandler<HTMLButtonElement>
}

const ImageBoard = (props: IProps) => {
    const imagePath = `${pathLocalHost}${props.image}`;
    const [open, setOpen] = useState(false);
    const user = useSelector<RootState, IUser | null | undefined>(state => state.usersState?.user);
    const {id} = useParams();

    return (
        <>
            <Col xs={24} sm={14} md={12} lg={8}
                 style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
                <Card
                    className='board'
                    hoverable
                    cover={<img alt="album" src={imagePath} className='image_board' onClick={() => setOpen(true)}/>}
                >
                    <Meta
                        title={props.without ? <p><Link to={props.path} style={{
                            color: "lightslategray",
                            fontSize: 30,
                            lineHeight: 1
                        }}>by {props.author}</Link></p> : null}
                        description={<p style={{color: "whitesmoke", fontSize: 20}}>{props.title}</p>}
                    />
                    {!props.without && id === user?._id ?
                        <button className='delete_btn' onClick={props.onDeleteHandler}>
                            <DeleteOutlined />
                        </button> : null}
                </Card>
            </Col>
            <Modal
                centered
                open={open}
                onOk={() => setOpen(false)}
                onCancel={() => setOpen(false)}
                width={700}
                footer={null}
                closeIcon={<CloseOutlined style={{color: 'whitesmoke', fontSize: 30}}/>}
                className='modal'
            >
                <img src={imagePath} alt="image_board" className='image_modal'/>
            </Modal>
        </>
    )
}

export default ImageBoard;