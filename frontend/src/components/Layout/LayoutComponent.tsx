import React from 'react';
import {Layout} from 'antd';
import {Link, Outlet, useNavigate} from "react-router-dom";
import './LayoutComponent.css';
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../index";
import {IUser} from "../../store/services/userSlice";
import {logoutUser} from "../../store/actions/userActions";

const {Header, Content} = Layout;

const LayoutComponent = () => {
    const user = useSelector<RootState, IUser | null | undefined>(state => state.usersState?.user);
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();

    return (
        <Layout>
            <Layout>
                <Header style={{
                    padding: 0,
                    background: "#101010",
                    display: "flex",
                    justifyContent: "space-between",
                    borderBottom: "1px solid whitesmoke"
                }}>
                    <Link to='/' className='logo'>Photo Gallery</Link>
                    {user ? <div className='menu_auth'>
                        <p className='user'>Hello {user?.username}</p>
                        <button className='login'
                                onClick={() => {
                                    dispatch(logoutUser({callback: () => navigate('/')}))
                                    navigate('/');
                                }}
                        >
                            Log out
                        </button>
                    </div> : <div className='menu_auth'>
                        <Link to='/register'><p className='auth'>Register</p></Link>
                        <Link to='/login'>
                            <button className='login'>Log in</button>
                        </Link>
                    </div>
                    }
                </Header>
                <Content
                    style={{
                        padding: 24,
                        minHeight: 280,
                        background: "#121212",
                    }}
                >
                    <Outlet/>
                </Content>
            </Layout>
        </Layout>
    );
};

export default LayoutComponent;