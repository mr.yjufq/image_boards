import {useEffect} from "react";
import {deleteImageBoard, getImageBoardsByAuthor} from "../../store/actions/imageBoardActions";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../index";
import {IImageBoards} from "../../store/services/imageBoardsSlice";
import ImageBoard from "../../components/ImageBoard/ImageBoard";
import {Link, useParams} from "react-router-dom";
import {message, Row} from "antd";
import './ShowAuthorBoards.css'
import {IUser} from "../../store/services/userSlice";

const ShowAuthorBoards = () => {
    const dispatch = useDispatch<AppDispatch>();
    const {id} = useParams();
    const imageBoards = useSelector<RootState, IImageBoards[] | undefined>(state => state.imageBoards?.imageBoards);
    const user = useSelector<RootState, IUser | null | undefined>(state => state.usersState?.user);

    const onDeleteHandler = (key: string) => {
        dispatch(deleteImageBoard({endpoint: key, user: id}));
        message.warning('Image board deleted', 3);
    };

    useEffect(() => {
        if (id) dispatch(getImageBoardsByAuthor(id));
    }, [dispatch, id]);

    const renderImageBoards = imageBoards?.map((element, index) => {
        return (
            <ImageBoard
                onDeleteHandler={() => onDeleteHandler(element?._id)}
                without={false}
                key={element?._id}
                author={element?.user_id?.username}
                title={element?.title}
                image={element?.image}
                path={`author/${element?.user_id?._id}`}
            />
        )
    });

    return (
        <div>
            <div className='main'>
                <div className='board_header'>
                    <p className='title_author'>{imageBoards ? `${imageBoards[0]?.user_id?.username}'s gallery` : 'Anonymous'}</p>
                    {id === user?._id ? <Link to='add-form' className='link_add'>Add new image board</Link> : null}
                </div>
                <Row className="image-boards-container" gutter={[16, 16]}>
                    {renderImageBoards}
                </Row>
            </div>
        </div>
    )
};

export default ShowAuthorBoards;