import {Button, Form, Input, message, Typography,} from 'antd';
import React, {ChangeEvent, FormEvent, FormEventHandler, useState} from 'react';
import {AppDispatch} from "../../index";
import {changeFileName} from "../../store/services/imageBoardsSlice";
import {useDispatch} from "react-redux";
import FileUploads from "../../components/UI/FileUpload/FileUpload";
import './AddForm.css'
import {postImageBoard} from "../../store/actions/imageBoardActions";
import {useNavigate} from "react-router-dom";

const {Text} = Typography;

interface PostForm {
    title: string,
    image: string
}

const AddForm = () => {
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();
    const [state, setState] = useState<PostForm>({
        title: "",
        image: ""
    });

    const inputChangeHandler: FormEventHandler<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> = (e) => {
        const target = e.target as HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement;
        const {id, value} = target;

        setState(prevState => {
            return {...prevState, [id]: value};
        });
    };

    const fileChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        const name = e.target.name;
        if (e.target.files) {
            const file = e.target.files[0];
            setState(prevState => ({
                ...prevState,
                [name]: file
            }))
        }
    };

    const submitFormHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formData = new FormData();
        for (let key in state) {
            formData.append(key, state[key as keyof typeof state]);
        }


        if (state.title.trim() === ''
            || typeof (state.image) === "string") {
            message.destroy()
            message.warning('title and image is required', 3);
        } else {
            message.destroy()
            message.success('You add image', 3);

            dispatch(postImageBoard(formData));

            setState(
                {
                    title: "",
                    image: ""
                }
            );

            dispatch(changeFileName(''));
            navigate(-1);
        }
    };

    return (
        <main className='main_form'>
            <Form
                onSubmitCapture={submitFormHandler}
                labelCol={{span: 4}}
                wrapperCol={{span: 14}}
                layout="horizontal"
                className="form_boards"
            >
                <Text style={{
                    textAlign: "center",
                    fontSize: "30px",
                    width: 400,
                    display: "inline-block",
                    marginBottom: 20,
                    color: "whitesmoke"
                }}>Add image boards</Text>
                <Form.Item>
                    <Input placeholder='Title' style={{width: 400}} id='title' onInput={inputChangeHandler}
                           value={state.title}/>
                </Form.Item>
                <FileUploads
                    name='image'
                    onChange={fileChangeHandler}
                />
                <Form.Item style={{maxWidth: 600, display: 'flex', flexDirection: "column", alignItems: "center"}}>
                    <Button htmlType='submit' className='add_button'>Add image board</Button>
                </Form.Item>
            </Form>
        </main>
    );
};

export default AddForm;