import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../index";
import {IImageBoards} from "../../store/services/imageBoardsSlice";
import ImageBoard from "../../components/ImageBoard/ImageBoard";
import {useEffect} from "react";
import {getImageBoards} from "../../store/actions/imageBoardActions";
import {Row} from "antd";
import './ShowImageBoards.css'

const ShowImageBoards = () => {
    const dispatch = useDispatch<AppDispatch>();
    const imageBoards = useSelector<RootState, IImageBoards[] | undefined>(state => state.imageBoards?.imageBoards);

    const renderImageBoards = imageBoards?.map((element, index) => {
        return (
            <ImageBoard
                without={true}
                key={element?._id}
                author={element?.user_id?.username}
                title={element?.title}
                image={element?.image}
                path={`author/${element?.user_id?._id}`}
            />
        )
    });

    useEffect(() => {
        dispatch(getImageBoards());
    }, [dispatch]);


    return (
        <div className='main'>
            <Row className="image-boards-container" gutter={[16, 16]}>
                {renderImageBoards}
            </Row>
        </div>
    )
};

export default ShowImageBoards;