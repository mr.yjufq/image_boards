import React, {FormEvent, FormEventHandler, useState} from 'react';
import {Button, Form, Input, message, Typography} from 'antd';
import './Register.css'
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../../index";
import {registerUser} from "../../../store/actions/userActions";
import {Link, useNavigate} from "react-router-dom";
import {setErrorRegister} from "../../../store/services/userSlice";

const {Text} = Typography;


const Register = () => {
    const errorRegister = useSelector<RootState, Error | null | undefined>(state => state.usersState.errorRegister);
    const navigate = useNavigate();
    const dispatch = useDispatch<AppDispatch>();

    const [state, setState] = useState<{ username: string, password: string }>({
        username: '',
        password: ''
    });


    const inputChangeHandler: FormEventHandler<HTMLInputElement> = (e) => {
        const target = e.target as HTMLInputElement | HTMLTextAreaElement;
        const {name, value} = target;

        setState(prevState => {
            return {...prevState, [name]: value};
        });

        dispatch(setErrorRegister(null));
    };

    const submitFormHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (state.username.trim() === '' || state.password.trim() === '') return;

        dispatch(registerUser({
            data: {...state},
            callback: () => navigate('/')
        }));
    };


    return (<>
            {errorRegister !== null ? message.warning('User already exist', 3) : null}
            <main className='register_main'>
                <Text style={{
                    fontSize: 30,
                    width: 600,
                    textAlign: "center",
                    fontWeight: "bold",
                    marginBottom: 100
                }}>
                    Register and get access to the publication of pictures
                </Text>
                <Form
                    onSubmitCapture={submitFormHandler}
                    name="basic"
                    labelCol={{span: 10}}
                    wrapperCol={{span: 10}}
                    style={{width: 400, display: "flex", alignItems: "center", flexDirection: "column", height: 400}}
                    autoComplete="off"
                >
                    <Text style={{
                        width: 400,
                        textAlign: "center",
                        fontSize: 15,
                        fontWeight: "bold",
                        borderBottom: "3px solid #4444",
                        marginBottom: 20
                    }}>
                        Enter login and password
                    </Text>
                    <Form.Item
                        name='username'
                        label={
                            <span style={{fontSize: 15, width: 300, fontWeight: "bold"}}>Create a username</span>
                        }
                        rules={[{required: true, message: errorRegister?.message}]}
                        labelCol={{span: 24}}
                    >
                        <Input style={{width: 400}} onInput={e => inputChangeHandler(e)} name="username"
                               value={state.username}/>
                    </Form.Item>
                    <Form.Item
                        name='password'
                        label={
                            <span style={{fontSize: 15, width: 300, fontWeight: "bold"}}>Create a password</span>
                        }
                        rules={[{required: true, message: errorRegister?.message}]}
                        labelCol={{span: 24}}
                    >
                        <Input.Password style={{width: 400}} onInput={e => inputChangeHandler(e)} name="password"
                                        value={state.password}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit"
                                style={{
                                    marginTop: 30,
                                    width: 250,
                                    padding: 30,
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    fontWeight: "bold",
                                    borderRadius: 30,
                                    color: "black",
                                    fontSize: 20,
                                    background: "#1ed760"
                                }}
                        >
                            Register
                        </Button>
                    </Form.Item>
                    <Text style={{
                        fontSize: 20,
                        width: 600,
                        textAlign: "center",
                        fontWeight: "bold",
                        marginBottom: 100
                    }}>
                        Are you has a account?
                        <Link to='/login' style={{color: "#1ed760", marginLeft: 5}}>
                            Log in
                        </Link>
                    </Text>
                </Form>
            </main>
        </>
    )
};

export default Register;