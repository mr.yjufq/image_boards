import React, {FormEvent, FormEventHandler, useState} from 'react';
import {Button, Form, Input, message, Typography} from 'antd';
import './Login.css'
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../../index";
import {loginUser} from "../../../store/actions/userActions";
import {useNavigate} from "react-router-dom";
import {IUser, setErrorLogin} from "../../../store/services/userSlice";

const {Text} = Typography;


const Login = () => {
    const errorLogin = useSelector<RootState, Error | null | undefined>(state => state.usersState.errorLogin);
    const user = useSelector<RootState, IUser | null | undefined>(state => state.usersState.user);
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();
    const [state, setState] = useState<{ username: string, password: string }>({
        username: '',
        password: ''
    });

        const inputChangeHandler: FormEventHandler<HTMLInputElement> = (e) => {
            const target = e.target as HTMLInputElement | HTMLTextAreaElement;
            const {name, value} = target;

            setState(prevState => {
                return {...prevState, [name]: value};
            });

            dispatch(setErrorLogin(null));
        };

    const submitFormHandler = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (state.username.trim() === '' || state.password.trim() === '') return;

        dispatch(loginUser({
            data: {...state},
            callback: () => navigate('/')
        }));
    };


    return (
        <>
            {errorLogin ? message.warning('user not found', 3) : null}
            <main className='login_main'>
                <Form
                    onSubmitCapture={submitFormHandler}
                    name="basic"
                    labelCol={{span: 10}}
                    wrapperCol={{span: 10}}
                    style={{
                        width: 700,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                        height: 700,
                        background: "#4444",
                        border: "2px solid whitesmoke",
                        borderRadius: 20
                    }}
                    autoComplete="off"
                >
                    <Text style={{
                        fontSize: 50,
                        width: 600,
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "whitesmoke",
                        marginBottom: 100
                    }}>
                        Log in in the system
                    </Text>
                    <Text style={{
                        width: 400,
                        textAlign: "center",
                        fontSize: 15,
                        fontWeight: "bold",
                        borderBottom: "3px solid #4444",
                        marginBottom: 10
                    }}>
                    </Text>
                    <Form.Item
                        name='username'
                        label={
                            <span style={{fontSize: 15, width: 1, fontWeight: "bold", color: "whitesmoke"}}>Login</span>
                        }
                        rules={[{required: true, message: errorLogin?.message}]}
                        labelCol={{span: 24}}
                    >
                        <Input style={{width: 400}} onInput={e => inputChangeHandler(e)} name="username"
                               value={state.username}/>
                    </Form.Item>
                    <Form.Item
                        name='password'
                        label={
                            <span style={{
                                fontSize: 15,
                                width: 1,
                                fontWeight: "bold",
                                color: "whitesmoke"
                            }}>Password</span>
                        }
                        rules={[{required: true, message: errorLogin?.message}]}
                        labelCol={{span: 24}}
                    >
                        <Input.Password style={{width: 400}} onInput={e => inputChangeHandler(e)} name="password"
                                        value={state.password}/>
                    </Form.Item>
                    <Text style={{
                        width: 400,
                        textAlign: "center",
                        fontSize: 15,
                        fontWeight: "bold",
                        borderBottom: "3px solid #4444",
                        marginTop: 10
                    }}>
                    </Text>
                    <Form.Item>
                        <Button type="primary" htmlType="submit"
                                style={{
                                    marginTop: 30,
                                    width: 250,
                                    padding: 30,
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    fontWeight: "bold",
                                    borderRadius: 30,
                                    color: "black",
                                    fontSize: 20,
                                    background: "#1ed760"
                                }}
                        >
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </main>
        </>
    )
};

export default Login;