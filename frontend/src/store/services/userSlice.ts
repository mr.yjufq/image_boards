import {createSlice} from "@reduxjs/toolkit";
import {loginUser, logoutUser, registerUser} from "../actions/userActions";

export interface IUser {
    _id: string
    username: string
    password: string
    token: string
}

export interface IUserState {
    user: IUser | null
    isLoading: boolean
    errorRegister: Error | null
    errorLogin: Error | null
    logoutError: Error | null
}

const initialState: IUserState = {
    user: null,
    isLoading: false,
    errorRegister: null,
    errorLogin: null,
    logoutError: null
}

const UsersSlices = createSlice({
    name: 'users',
    initialState,
    reducers: {
        setErrorRegister: (state, action) => {
            state.errorRegister = action.payload;
        },
        setErrorLogin: (state, action) => {
            state.errorLogin = action.payload;
        },
        setUser: (state, action) => {
            state.user = action.payload;
        },
        setLogoutError: (state, action) => {
            state.logoutError = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(registerUser.pending, (state) => {
                state.errorRegister = null;
                state.isLoading = true;
            })
            .addCase(registerUser.fulfilled, (state) => {
                state.isLoading = false;
            })
            .addCase(registerUser.rejected, (state) => {
                state.isLoading = false;
            })
            .addCase(loginUser.pending, (state) => {
                state.errorLogin = null;
                state.isLoading = true;
                state.user = null;
            })
            .addCase(loginUser.fulfilled, (state) => {
                state.isLoading = false;
            })
            .addCase(loginUser.rejected, (state) => {
                state.isLoading = false;
            })
            .addCase(logoutUser.pending, (state) => {
                state.logoutError = null;
                state.isLoading = true;
                state.user = null;
            })
            .addCase(logoutUser.fulfilled, (state) => {
                state.isLoading = false;
            })
            .addCase(logoutUser.rejected, (state) => {
                state.isLoading = false;
            })
    }
});

export const {
    setErrorLogin,
    setErrorRegister,
    setUser,
    setLogoutError
} = UsersSlices.actions;
export default UsersSlices.reducer;
