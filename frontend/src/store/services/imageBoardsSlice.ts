import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {getImageBoards, getImageBoardsByAuthor, postImageBoard} from "../actions/imageBoardActions";

export interface IImageBoards {
    _id: string,
    title: string,
    user_id: {
        _id: string
        username: string
    },
    image: string
}

export interface IImageBoardsState {
    imageBoards: IImageBoards[];
    isLoading: boolean
    error: Error | null
    filename: string
}

const initialState: IImageBoardsState = {
    imageBoards: [],
    isLoading: false,
    error: null,
    filename: ''
}

const ImageBoardsSlice = createSlice({
    name: 'imageBoards',
    initialState,
    reducers: {
        changeFileName: (state, action: PayloadAction<string>) => {
            state.filename = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getImageBoards.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(getImageBoards.fulfilled, (state, action) => {
                state.imageBoards = action.payload;
                state.isLoading = false;
                state.error = null;
            })
            .addCase(getImageBoards.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
            .addCase(getImageBoardsByAuthor.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(getImageBoardsByAuthor.fulfilled, (state, action) => {
                state.imageBoards = action.payload;
                state.isLoading = false;
                state.error = null;
            })
            .addCase(getImageBoardsByAuthor.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
            .addCase(postImageBoard.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(postImageBoard.fulfilled, (state) => {
                state.isLoading = false;
                state.error = null;
            })
            .addCase(postImageBoard.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
    }
});

export const {
    changeFileName
} = ImageBoardsSlice.actions;
export default ImageBoardsSlice.reducer;