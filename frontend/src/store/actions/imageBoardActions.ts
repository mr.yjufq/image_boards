import {createAsyncThunk} from "@reduxjs/toolkit";
import instance from "../../API/createAxios";
import {AxiosRequestConfig, AxiosResponse} from "axios";

export const getImageBoards = createAsyncThunk(
    'imageBoards.get',
    async () => {
        const res = await instance.get<AxiosRequestConfig, AxiosResponse>("/boards");
        if (res !== null) {
            const data = Object.keys(res.data).map(key => {
                return {...res.data[key]}
            });

            return data;
        } else {
            return [];
        }
    }
);

export const getImageBoardsByAuthor = createAsyncThunk(
    'imageBoardsByAuthor.get',
    async (payload: string) => {
        const res = await instance.get<AxiosRequestConfig, AxiosResponse>("/boards?user_id=" + payload);
        if (res !== null) {
            const data = Object.keys(res.data).map(key => {
                return {...res.data[key]}
            });

            return data;
        } else {
            return [];
        }
    }
);
export const postImageBoard = createAsyncThunk(
    'imageBoard.post',
    async (payload: FormData) => {
        const res = await instance.post<AxiosRequestConfig, AxiosResponse>('/boards', payload);
        return res.data;
    }
);

export const deleteImageBoard = createAsyncThunk(
    'imageBoard.delete',
    async (payload: {user: string | undefined, endpoint: string}, {dispatch}) => {
        const res = await instance.delete<AxiosRequestConfig, AxiosResponse>('/boards/' + payload.endpoint);
        if (payload.user) await dispatch(getImageBoardsByAuthor(payload.user));
        return res.data;
    }
);
