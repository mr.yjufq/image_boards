import {createAsyncThunk} from "@reduxjs/toolkit";
import instance from "../../API/createAxios";
import {AxiosRequestConfig, AxiosResponse} from "axios";
import {setErrorLogin, setErrorRegister, setLogoutError, setUser} from "../services/userSlice";
import {AppDispatch, RootState} from "../../index";

interface IUser {
    data: {
        username: string,
        password: string
    },
    callback: () => void
}

interface IPayload {
    callback: () => void
}


export const registerUser = createAsyncThunk(
    'register.user',
    async (payload: IUser, {dispatch}) => {
        await instance.post<AxiosRequestConfig, AxiosResponse>('/users', payload.data)
            .then(() => payload.callback())
            .catch(e => {
                if (e?.response?.data) dispatch(setErrorRegister(e.response.data));
                throw e;
            });
    }
);


export const loginUser = createAsyncThunk(
    'login.user',
    async (payload: IUser, {dispatch}) => {
        await instance.post<AxiosRequestConfig, AxiosResponse>('/users/login', payload.data)
            .then(res => {
                dispatch(setUser(res.data))
                payload.callback()
                return res.data;
            })
            .catch(e => {
                if (e?.response?.data) dispatch(setErrorLogin(e.response.data));
                else dispatch(setErrorLogin(e));
                throw e;
            });
    }
);

export const logoutUser = createAsyncThunk<void, IPayload, { dispatch: AppDispatch, state: RootState }>(
    'users/logout',
    async (payload: IPayload, {dispatch, getState}) => {
        const userToken = getState().usersState.user?.token;

        await instance
            .delete(
                '/users/logout',
                {headers: {Authorization: userToken}}
            )
            .then(res => {
                dispatch(setUser(null));
                payload.callback();
            })
            .catch(e => {
                if (e?.response?.data) dispatch(setLogoutError(e.response.data));
                else dispatch(setLogoutError(e));
                throw e;
            })
    }
);



